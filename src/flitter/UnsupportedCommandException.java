/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flitter;

/**
 *
 * @author Zack
 */
public class UnsupportedCommandException extends Exception{

    public UnsupportedCommandException() {
    }
    
    public UnsupportedCommandException(String msg) {
        super(msg);
    }
    
}
