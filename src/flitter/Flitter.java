package flitter;

import java.io.IOException;

public class Flitter {

    public static void main(String[] args) throws IOException {
        
        FlitterCommandParser    parser  ;   //Parses input into command
        FlitterCommand          command ;   //Valid user request
        FlitterEngine           engine  ;   //Executes commands
        
        parser  = new FlitterCommandParser();
        engine  = new FlitterEngine();
        command = new QuitCommand();  //Initialize to a quit command
        
        //Process commands until user quits
        do {
            try {
            parser.getRawInput();           //Get user input.
            command = parser.getCommand();  //Get command from parser
            engine.exectue(command);        //Execute Command
            }
            catch (UnsupportedCommandException ex) {    //Check for unsupported command
                if(!(command instanceof QuitCommand)) {
                    System.out.println("Invalid Command");
                }
            }
        }
        while(!(command instanceof QuitCommand));
    }
}
