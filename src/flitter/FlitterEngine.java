/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flitter;

/**
 *
 * @author Zack
 */
class FlitterEngine {

    void exectue(FlitterCommand command) throws UnsupportedCommandException {
        //Execute valid command or throw exception
        if(command instanceof LogInCommand) {
            System.out.println("Login command");
        }
        else if (command instanceof ShowCurrentUserCommand) {
            System.out.println("Show current user command");
        }
        else if (command instanceof FollowCommand) {
            System.out.println("Follow command");
        }
        else if (command instanceof AddUserCommand) {
            System.out.println("Add User Command");
        }
        else if (command instanceof UnfollowCommand) {
            System.out.println("Unfollow Command");
        }
        else if (command instanceof ShowFleetsCommand) {
            System.out.println("ShowFleets");
        }
        else {
            throw new UnsupportedCommandException();
        }
    }
    
}
