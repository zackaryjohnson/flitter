/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flitter;

/**
 *
 * @author Zack
 */
class FlitterCommand {

    public static void displayCommands() {
        System.out.println("");
        System.out.println("Flitter commands and thier usage");
        System.out.println("--------------------------------");
        System.out.println("Create Account    - adduser @handle firstname lastname \"City,ST\"");
        System.out.println("Show Current User - whoami?");
        System.out.println("Follow Somebody   - follow @handle");
        System.out.println("Unfollow Somebody - unfollow @handle");
        System.out.println("Show Fleet List   - showfleets [fleets to show] [offset]");
        System.out.println("Log In            - login username password");
    }
    
}
