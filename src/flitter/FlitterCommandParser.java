package flitter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

class FlitterCommandParser {

    private String rawUserInput;
    
    void getRawInput() throws IOException {
        Scanner userInput;

        BufferedReader consoleIn = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("type command or h for help: ");
        rawUserInput = consoleIn.readLine();
        rawUserInput = rawUserInput.trim();
    }

    boolean validCommand() {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    FlitterCommand getCommand() {
        if(rawUserInput.compareTo("adduser") == 0) {
            return new AddUserCommand();
        }
        else if(rawUserInput.compareTo("follow") == 0) {
            return new FollowCommand();
        }
        else if(rawUserInput.compareTo("login") == 0) {
            return new LogInCommand();
        }
        else if(rawUserInput.compareTo("quit") == 0) {
            return new QuitCommand();
        }
        else if(rawUserInput.compareTo("whoami?") == 0) {
            return new ShowCurrentUserCommand();
        }
        else if(rawUserInput.compareTo("showfleets") == 00) {
            return new ShowFleetsCommand();
        }
        else if(rawUserInput.compareTo("unfollow") == 0) {
            return new UnfollowCommand();
        }
        else {
            return new InvalidCommand();
        }
    }
    
}
